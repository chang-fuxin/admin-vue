import request from "../utils/request";

/**
 * 用户列表
 */
export function userList(params) {
  return request({
    url: `/FindAllUser`,
    method: "GET",
    params
  })
}

/**
 * 修改用户状态
 */
export function updateUserStatus(params) {
  return request({
    url: `/UpdateUser`,
    method: "POST",
    params
  })
}
/**
 * 删除类别
 */
export function deleteUser(id){
  return request({
    url: `/DeleteUser/`+id,
    method: "delete"
  })
}

/**
 * 获取一周的注册数据
 */
export function registerWeek() {
  return request({
    url: `/getRegisterWeek`
  })
}

/**
 * 获取一周的注册数据
 */
export function registerLast() {
  return request({
    url: `/getRegisterLast`
  })
}

/**
 * 获取一周的注册数据
 */
export function restPhone() {
  return request({
    url: `/restPhone`
  })
}

/**
 * 获取一周的注册数据
 */
export function getUsers(params) {
  return request({
    url: `/download`,
    params
  })
}
