import request from "../utils/request";

/**
 * 类别列表
 */
export function categorys() {
    return request({
        url: `/FindAllCategory`,
    })
}

/**
 * 新增类别
 */
export function addCategory(data) {
    return request({
        url: `/AddCategory`,
        method: "post",
        data
    })
}

/**
 * 删除类别
 */
export function deleteCategory(id){
    return request({
        url: `/DeleteCategory/`+id,
        method: "delete"
    })
}

/**
 * 修改类别
 */
export function upDateCategory(data){
  return request({
    url: `/UpDateCategory`,
    method: "post",
    data
  })
}
