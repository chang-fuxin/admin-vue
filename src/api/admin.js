import request from "../utils/request";

/**
 * 用户登录
 */
export function login(data){
    return request({
        url: `/Login`,
        method: 'POST',
        data
    })
}

/**
 * 获取验证码
 */
export function captcha(){
    return request({
        url: `/Captcha`,
    })
}

/**
 * 退出登录
 */
export function verificationUser(data){
  return request({
    url: `/VerificationUser`,
    method: `POST`,
    data
  })
}
/**
 * 获取用户登录状态
 */
export function safetyQuit(data){
  return request({
    url: `/SafetyQuit`,
    method: `POST`,
    data
  })
}
